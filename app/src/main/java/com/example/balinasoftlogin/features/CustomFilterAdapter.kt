package com.example.balinasoftlogin.features

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import java.util.*


@Suppress("UNCHECKED_CAST")
class CustomFilterAdapter(context: Context, private val viewResourceId: Int, private var items: ArrayList<String>) : ArrayAdapter<String>(context, viewResourceId, items) {

    private var itemsAll: ArrayList<String> = items.clone() as ArrayList<String>
    private var suggestions: ArrayList<String> = ArrayList()

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var v: View? = convertView
        if (v == null) {
            val vi = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            v = vi.inflate(viewResourceId, null)
        }
        val customer = items[position]
        val customerNameLabel = v as TextView?
        if (customerNameLabel != null) {
            customerNameLabel.text = customer
        }
        return v
    }

    override fun getFilter(): Filter {
        return nameFilter
    }


    private var nameFilter: Filter = object : Filter() {
        override fun convertResultToString(resultValue: Any): String {
            return resultValue as String
        }

        override fun performFiltering(enteredText: CharSequence?): Filter.FilterResults {
            if (enteredText != null) {
                val character = enteredText.toString()
                if (character.indexOf("@") != -1) {
                    val compCharacter = character.substring(character.indexOf("@"))
                    val checkCharacter: String = try {
                        character.substring(0, character.indexOf("@"))
                    } catch (e: Exception) {
                        ""
                    }
                    suggestions.clear()
                    for (customer in itemsAll) {
                        if (customer.toLowerCase().startsWith(compCharacter.toLowerCase())) {
                            suggestions.add(checkCharacter + customer)
                        }
                    }
                    val filterResults = FilterResults()
                    filterResults.values = suggestions
                    filterResults.count = suggestions.size
                    return filterResults
                } else {
                    return FilterResults()
                }
            } else {
                return FilterResults()
            }
        }

        @SuppressLint("ShowToast")
        override fun publishResults(enteredText: CharSequence?, results: FilterResults) {
            val filteredList: ArrayList<String>? = results.values as? ArrayList<String>
            if (results.count > 0) {
                this@CustomFilterAdapter.clear()
                if (filteredList != null) {
                    for (c in filteredList) {
                        this@CustomFilterAdapter.add(c)
                    }
                }
                notifyDataSetChanged()
            }
        }
    }
}