package com.example.balinasoftlogin.features

import android.util.Patterns
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Single
import java.util.*

class CheckedEditText {


    fun checkGreaterThenSixObs(): ObservableTransformer<String, String> {
        return ObservableTransformer { observable ->
            observable.flatMap {
                Observable.just(it).map { it.trim() }
                    .filter { it.length > 7 }
                    .singleOrError()
                    .onErrorResumeNext {
                        if (it is NoSuchElementException) {
                            Single.error(Exception("Длина должна быть больше 7"))
                        } else {
                            Single.error(it)
                        }
                    }.toObservable()
            }
        }
    }

    fun checkLessThenThirty(): ObservableTransformer<String, String> {
        return ObservableTransformer { observable ->
            observable.flatMap {
                Observable.just(it).map { it.trim() }
                    .filter { it.length < 30 }
                    .singleOrError()
                    .onErrorResumeNext {
                        if (it is NoSuchElementException) {
                            Single.error(java.lang.Exception("Длина не может быть больше 30"))
                        } else {
                            Single.error(it)
                        }
                    }.toObservable()
            }
        }
    }

    fun checkEmailPattern(): ObservableTransformer<String, String> {
        return ObservableTransformer { observable ->
            observable.flatMap {
                Observable.just(it).map { it.trim() }
                    .filter { Patterns.EMAIL_ADDRESS.matcher(it).matches() }
                    .singleOrError()
                    .onErrorResumeNext {
                        if (it is NoSuchElementException) {
                            Single.error(java.lang.Exception("Некорректный email"))
                        } else {
                            Single.error(it)
                        }
                    }.toObservable()
            }
        }
    }

    fun checkDomainName(): ObservableTransformer<String, String> {
        return ObservableTransformer { observable ->
            observable.flatMap {
                Observable.just(it)
                        .map {  it.substringAfter("@") }
                        .filter { Patterns.DOMAIN_NAME.matcher(it).matches() }
                        .singleOrError()
                        .onErrorResumeNext {
                            if (it is NoSuchElementException) {
                                Single.error(java.lang.Exception("Некорректный домен"))
                            } else {
                                Single.error(it)
                            }
                        }.toObservable()
            }
        }
    }

    inline fun retryWhenError(crossinline onError: (ex: Throwable) -> Unit): ObservableTransformer<String, String> =
        ObservableTransformer { observable ->
            observable.retryWhen { errors ->
                errors.flatMap {
                    onError(it)
                    Observable.just("")
                }
            }
        }
}