package com.example.balinasoftlogin.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.balinasoftlogin.R


class RegistrationLoginContainerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reg_log_container)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.logRegContainer, RegistrationFragment().newInstance())
                    .commit()
        }

    }
}
