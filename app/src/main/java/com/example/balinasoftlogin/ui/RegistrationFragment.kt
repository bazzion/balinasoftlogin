package com.example.balinasoftlogin.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.balinasoftlogin.R
import com.example.balinasoftlogin.features.CheckedEditText
import com.example.balinasoftlogin.features.CustomFilterAdapter
import com.example.balinasoftlogin.network.ApiLogin
import com.example.balinasoftlogin.network.NetworkClient
import com.example.balinasoftlogin.network.model.BadResponse
import com.example.balinasoftlogin.network.model.UserDto
import com.google.gson.Gson
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_registration.*
import org.koin.android.ext.android.inject
import retrofit2.HttpException
import java.util.*
import java.util.concurrent.TimeUnit


class RegistrationFragment : Fragment() {


    private val checkedEditText: CheckedEditText by inject()
    private val retrofit: NetworkClient by inject()
    private val disposable = CompositeDisposable()
    private var fullField: String = ""
    private var baseUrl: String = ""

    fun newInstance(): RegistrationFragment {
        val regFragment = RegistrationFragment()
        val args = Bundle()
        regFragment.arguments = args
        return regFragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        fullField = resources.getString(R.string.empty_field)
        baseUrl = resources.getString(R.string.base_url)


        return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val countries: ArrayList<String> =
                (resources.getStringArray(R.array.listOfPopularEmails).toCollection(ArrayList()))
        CustomFilterAdapter(view.context, android.R.layout.simple_list_item_1, countries).also { adapter ->
            emailAutoCompleteEditText.setAdapter(adapter)
        }

        setTextChecker()

        registerButton.setOnClickListener {
            if (emailAutoCompleteEditText.text.toString() == "" && passwordAutoCompleteTextView.text.toString() == "") {

                emailInputLayout.error = fullField
                passwordInputLayout.error = fullField
            } else if (emailAutoCompleteEditText.text.toString() == "") {
                emailInputLayout.error = fullField
            } else if (passwordAutoCompleteTextView.text.toString() == "") {
                passwordInputLayout.error = fullField
            } else if (emailInputLayout.error != null) {
                //
            } else if (passwordInputLayout.error != null) {
                //
            } else {
                postRequest(it)
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun postRequest(it: View) {
        val userDto = UserDto(emailAutoCompleteEditText.text.toString(), passwordAutoCompleteTextView.text.toString())

        disposable.add(retrofit.getClient(ApiLogin::class.java, baseUrl)
                .loginIUser(userDto)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { request -> Toast.makeText(context, request.data.token, Toast.LENGTH_LONG).show() },
                        { error -> processError(error, it) }
                ))
    }

    private fun processError(error: Throwable, view: View) {

        val badResponse: BadResponse = Gson().fromJson((error as HttpException).response().errorBody()!!.charStream(), BadResponse::class.java)
        Toast.makeText(view.context, badResponse.error, Toast.LENGTH_LONG).show()
    }

    override fun onPause() {
        super.onPause()
        disposable.let { if (!it.isDisposed) it.dispose() }
    }

    private fun setTextChecker() {

        RxTextView.afterTextChangeEvents(emailAutoCompleteEditText)
                .skipInitialValue()
                .map {
                    emailInputLayout.error = null
                    registerButton.isEnabled = true
                    it.view().text.toString()
                }
                .debounce(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .compose(verifyEmailPattern)
                .compose(verifyDomainName)
                .compose(checkedEditText.retryWhenError {
                    emailInputLayout.error = it.message
                    registerButton.isEnabled = false
                })
                .subscribe()

        RxTextView.afterTextChangeEvents(passwordAutoCompleteTextView)
                .skipInitialValue()
                .map {
                    passwordInputLayout.error = null
                    registerButton.isEnabled = true
                    it.view().text.toString()
                }
                .debounce(1, TimeUnit.SECONDS).observeOn(AndroidSchedulers.mainThread())
                .compose(lengthGreaterThanSix)
                .compose(lengthLessThanThirty)
                .compose(checkedEditText.retryWhenError {
                    passwordInputLayout.error = it.message
                    registerButton.isEnabled = false
                })
                .subscribe()


    }

    private val verifyDomainName = checkedEditText.checkDomainName()
    private val lengthGreaterThanSix = checkedEditText.checkGreaterThenSixObs()
    private val lengthLessThanThirty = checkedEditText.checkLessThenThirty()
    private val verifyEmailPattern = checkedEditText.checkEmailPattern()
}