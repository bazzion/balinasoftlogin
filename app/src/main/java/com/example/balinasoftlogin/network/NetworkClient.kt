package com.example.balinasoftlogin.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class NetworkClient {


    private fun getRetrofit(baseUrl: String): Retrofit {
        val builder = OkHttpClient.Builder()
        val okHttpClient = builder.build()
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
    }

    fun <T> getClient(service: Class<T>, baseUrl: String): T {
        return getRetrofit(baseUrl).create(service)
   }
}