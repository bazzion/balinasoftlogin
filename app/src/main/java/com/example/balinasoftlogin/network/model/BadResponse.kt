package com.example.balinasoftlogin.network.model

data class BadResponse(
    val status: Int,
    val error: String,
    val valid: Valid
) {
    data class Valid(
        val field: String,
        val message: String
    )
}