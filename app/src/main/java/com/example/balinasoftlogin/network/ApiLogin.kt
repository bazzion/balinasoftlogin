package com.example.balinasoftlogin.network

import com.example.balinasoftlogin.network.model.SuccessfulResponse
import com.example.balinasoftlogin.network.model.UserDto
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiLogin {

    @POST("api/account/signup")
    fun loginIUser(@Body userDto: UserDto): Observable<SuccessfulResponse>
}