package com.example.balinasoftlogin.network.model

data class SuccessfulResponse(
    val status: Int,
    val data: Data
) {
    data class Data(
        val userId: Int,
        val login: String,
        val token: String
    )
}