package com.example.balinasoftlogin

import android.app.Application
import com.example.balinasoftlogin.koinModules.checkModule
import org.koin.android.ext.android.startKoin

class App : Application() {


    override fun onCreate() {
        super.onCreate()

        startKoin(this@App, listOf(checkModule))
    }
}