package com.example.balinasoftlogin.koinModules

import com.example.balinasoftlogin.features.CheckedEditText
import com.example.balinasoftlogin.network.NetworkClient
import org.koin.dsl.module.module

val checkModule = module {
    single { CheckedEditText() }
    single { NetworkClient() }
}

